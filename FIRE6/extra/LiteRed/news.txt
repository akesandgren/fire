**New in LiteRed v1.81:

*Improved/corrected functionality:
	Bug with wrong `SolvejSector::leak` message is corrected. 

**New in LiteRed v1.8:

*New function
	`jsOrder[basis,1,0,...]` --- Gives ordering for a specific sector. Can be modified by `basis/:jsOrder[basis,1,0,...]={2,1,3,...}`, where `{2,1,3,...}` is a permutation. Sometimes changing ordering may help in finding reduction rules. You may want to try `basis/:jsOrder[basis,1,0,...]=PermutationList[RandomPermutation[n]]` with `n` being the number of indices.
	
*New function
	`ToMIsRules[jjs__]` --- gives a rule to pass to new master integrals. If `jjs` is incomplete, they replace as many simplest masters as possible. If the set `jjs` is linear dependent, `$Failed` is returned.

*New function
	`AtoLeft[expr]` pulls all A operators to the left (see also ToAB).

*Improved/corrected functionality:
	You may now use shortcut `sp[v]=s` which is expanded as `sp[v,v]=s`.


*Improved/corrected functionality:
	Several minor bugs corrected.
	
**New in LiteRed v1.6:

*Improved/corrected functionality:
	Option `CutDs` of `AnalyzeSectors` now persists for `FindSymmetries` and prevents `LiteRed` from finding symmetries mapping cut denominators to uncut ones and vice versa.
	The flag list of cut denominators is available as `CutDs[basis]`

*New function IdentifyMIs:

	`IdentifyMIs[basis]` uses Feynman parametrization approach to find possible mapings between masters.
	Options:
		`IBPReduce->True|False:True` determines whether to perform ibp reduction after mapping (since the result of the mapping is not necessarily a master)
		`Save->True|False:False` determines whether to exclude mapped master integrals from `MIs[basis]` and to append found rules to jRules of the corresponding sector (which means they will be used in IBPReduce).

*New function
	`IBPLI[basis]` --- Joined lists if IBP[basis] and LI[basis], as a pure function

*New syntax
	`IBP[basis,n1,n2,...]`
	`LI[basis,n1,n2,...]`
	`IBPLI[basis,n1,n2,...]`

*New functions `ToAB`, `FromAB`:
	`ToAB[expr,j[basis,n1,n2...]]` gives the AB-form of expr.
	`ToAB[expr_Function,basis]` is a shortcut for `ToAB[expr[n1,n2,...],j[basis,n1,n2...]]`.
	`ToAB` threads through lists in `expr`.

	`FromAB[expr,basis]` gives the Function-form of `expr`.
	`FromAB[expr_Function,j[basis,n1,n2,...]]` is a shortcut for `FromAB[expr,basis][n1,n2,\[Ellipsis]]`.
	`FromAB` threads through lists in `expr`.
	
	`A` and `B` are protected symbols now.
*New functions `ABIBP`, `ABLI`, `ABIBPLI`:
	`ABIBP[basis]`, `ABLI[basis]`, `ABIBPLI[basis]` give the AB-form of the corresponding identities.

 
*New function `FeynParUFFunction`:
	`FeynParUFFunction[basis]` gives, as a pure function, the list {U,F} of polynomials, entering the Feynman parametrization of the integrals in the highest sector  of the basis.
	`FeynParUFFunction[js[...]]` gives U and F polynomials, entering the Feynman parametrization of the integrals in the given sector.
	`FeynParUFFunction[{dens},{lms}]` does the same for the integrals with denominators {dens} and loop momenta {lms}.

*New function GramPFunction:
	`GramPFunction[basis]` gives a Gram polynomial G(l1,...,lL,p1,...,pE) as a a pure function  of denominators.

*New convenience tool `Information[basis]`:
	`Information[basis]` --- just try it, self explaining.

*New shortcuts:	
	`basis[j,indices]`:=`j[basis,indices]`;
	`basis[js,indices]`:=`js[basis,indices]`;
	Attached syntax information alleviates input (if the number of indices is wrong, it gives a syntax mark).

*New syntax:
	`MIs[basis,0,1,...]` or `MIs[js[basis,0,1,...]]` returns the list of masters of the given setor.

*New functions `ToDShifts`, `FromDShifts`:
	`ToDShifts[expr]` gets rid of numerators in expr at the expense of shifts of dimension. The result is a list {d-dimensional part,(d+2)-dimensional part with the replacement d->d-2,(d+4)-dimensional part with the replacement d->d-4,...}. Such a form allows one to make a reduction prior to 'inverse' operator FromDShifts.
	
	`FromDShifts[{expr0,expr1,expr2,...}]` uses `LoweringDRR` to combine pieces from shifted dimensions. Up to the reduction, this operation is inverse of ToDShifts.

*New syntax:
	`Dinv[expr_,s_Symbol]`  gives the derivative of `expr` (containing `j[...]`) with respect to the variable `s`.
