/** @file point.cpp
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 *  It contains the basic point class corresponding to a Feynman integral
 */

#include "point.h"

vector<set<vector<t_index> > > point::preferred;
vector<set<point_fast> > point::preferred_fast;
// numbered with sector numbers

bool point::print_g = false;

// we had to move them here to avoid circular dependencies
map<unsigned short, vector<pair<vector<t_index>, vector<pair<vector<pair<COEFF, point_fast> >, t_index> > > > > point::ibases;
//        sector       variants    permutation    product     sum         coefficient   indices       powers

map<unsigned short, pair<vector<t_index>, vector<pair<vector<pair<COEFF, point_fast> >, t_index> > > > point::dbases;
//        sector           permutation    product     sum         coefficient   indices       powers


point_fast::point_fast() {
    memset(buf, 0, MAX_IND);
}

point_fast::~point_fast() = default;

point_fast &point_fast::operator=(const point_fast &p) {
    memcpy(buf, p.buf, MAX_IND);
    return *this;
}

point_fast::point_fast(const point &p) {
    memset(buf, 0, MAX_IND);
    vector<t_index> v = p.get_vector();
    t_index *pos = buf;
    for (auto itr = v.begin(); itr != v.end(); ++itr, ++pos) *pos = *itr;
}

point_fast::point_fast(const vector<t_index> &v) {
    memset(buf, 0, MAX_IND);
    t_index *pos = buf;
    for (auto itr = v.begin(); itr != v.end(); ++itr, ++pos) *pos = *itr;
}

point_fast::point_fast(const point_fast &p) {
    memcpy(buf, p.buf, MAX_IND);
}

SECTOR point_fast::sector_fast() const {
    SECTOR result = 0;
    const t_index *pos_begin = this->buf;
    const t_index *pos_end = this->buf + common::dimension;
    while (pos_begin != pos_end) {
        if ((*pos_begin) > 0) {
            result = ((result << 1) ^ 1);
        } else {
            result = result << 1;
        }
        ++pos_begin;
    }
    return result;
}

point_fast point_fast::degree() const {
    point_fast result;
    const t_index *pos_old = this->buf;
    t_index *pos_new = result.buf;
    for (size_t i = 0; i != common::dimension; ++i, ++pos_new, ++pos_old)
        if ((*pos_old) > 0) {
            *pos_new = (*pos_old) - 1;
        } else {
            *pos_new = -(*pos_old);
        }
    return result;
}

bool over_fast(const point_fast &l, const point_fast &r) {
    for (unsigned int i = 0; i != common::dimension; ++i) {
        if (l.buf[i] < r.buf[i]) return false;
    }
    return true;
}

point::point(const point &p) {
#ifndef SMALL_POINT
    reinterpret_cast<uint64_t *>(this->ww)[2] = reinterpret_cast<const uint64_t *>(p.ww)[2];
#endif
    reinterpret_cast<uint64_t *>(this->ww)[1] = reinterpret_cast<const uint64_t *>(p.ww)[1];
    reinterpret_cast<uint64_t *>(this->ww)[0] = reinterpret_cast<const uint64_t *>(p.ww)[0];
}

point &point::operator=(const point &p) {
#ifndef SMALL_POINT
    reinterpret_cast<uint64_t *>(this->ww)[2] = reinterpret_cast<const uint64_t *>(p.ww)[2];
#endif
    reinterpret_cast<uint64_t *>(this->ww)[1] = reinterpret_cast<const uint64_t *>(p.ww)[1];
    reinterpret_cast<uint64_t *>(this->ww)[0] = reinterpret_cast<const uint64_t *>(p.ww)[0];
    return *this;
}

point::point() {
#ifndef SMALL_POINT
    reinterpret_cast<uint64_t *>(this->ww)[2] = 0;
#endif
    reinterpret_cast<uint64_t *>(this->ww)[1] = 0;
    reinterpret_cast<uint64_t *>(this->ww)[0] = 0;
}


int point::level() const {
    return positive_index(common::ssectors[s_number()]);
}

point::point(const vector<t_index> &v, virt_t virt, SECTOR ssector) {
#ifndef SMALL_POINT
    reinterpret_cast<uint64_t *>(ww)[2] = 0;
#endif
    reinterpret_cast<uint64_t *>(ww)[1] = 0;
    reinterpret_cast<uint64_t *>(ww)[0] = 0;

    SECTOR s = ssector;
    if (s == static_cast<SECTOR>(-1)) s = sector_fast(v);
    unsigned short sn;
    if (s == static_cast<SECTOR>(-2)) {
        sn = 1;
    } else {
        sn = common::sector_numbers_fast[s];
    }

    *h1p() = (((uint64_t) sn) << 1);
    if (sn == 0) {
        cout << s << endl;
        cout << endl << "Sector 0" << endl << "Possible error in symmetries" << endl;
        throw 1;
    }

    if (virt != 0) {
        *((virt_t * )(ww + sizeof(point) - 7)) = (virt);
        ww[sizeof(point) - 3] = 0;
        return;
    }

    if ((sn == 1) || (sn == common::virtual_sector)) {
        for (unsigned int i = 0; i != common::dimension; ++i) {
#ifdef SMALL_POINT
            if (i & 1)
                ww[sizeof(point) - (i>>1) - 3] |= (8 + v[i]);
            else
                ww[sizeof(point) - (i>>1) - 3] = (8 + v[i])<<4;
#else
            ww[MAX_IND - i - 1] = (common::small ? 8 : 128) + v[i];
#endif
        }
    } else {
        t_index *ordering_now = common::orderings_fast[s];
        t_index degrees[MAX_IND];
        t_index *pos = (t_index *) degrees;
        for (auto i = v.begin(); i != v.end(); ++pos, ++i) {
            if ((*i) > 0) {
                *pos = (*i) - 1;
            } else {
                *pos = -(*i);
            }
        }
        for (unsigned int i = 0; i != common::dimension; ++i) {
#ifdef SMALL_POINT
            unsigned char pr = i ? 0 : 128; // for first byte it is indication that it is not virtual
#else
            unsigned char pr = 1;  // it should be at least 1 to be higher than virtual
#endif
            for (unsigned int j = 0; j != common::dimension; ++j) {
                pr += ordering_now[i * common::dimension + j] * (degrees[j]);
            }
#ifdef SMALL_POINT
            // first byte is also used to understand whether the point is virtual, so it cannot store 0
            // moreover the first product is the bigest
            // hence i = 0 goes to one byte, then pairwise
            // hence odd numbers go to high parts of byte starting from size-4
            // even numbers go to low parts of byte starting from size-3
            if (i & 1)
                ww[sizeof(point) - (i>>1) - 4] = pr<<4;
            else
                ww[sizeof(point) - (i>>1) - 3] |= pr;
#else
            ww[MAX_IND - i - 1] = pr;
#endif
        }
        if (preferred[sn].find(v) == preferred[sn].end()) {
            *h1p() |= ((unsigned short) 1);
        }
    }
}

point::point(const point_fast &pf, SECTOR ssector) {
#ifndef SMALL_POINT
    reinterpret_cast<uint64_t *>(ww)[2] = 0;
#endif
    reinterpret_cast<uint64_t *>(ww)[1] = 0;
    reinterpret_cast<uint64_t *>(ww)[0] = 0;

    SECTOR s = ssector;
    if (s == static_cast<SECTOR>(-1)) { s = pf.sector_fast(); }
    unsigned short sn = common::sector_numbers_fast[s];
    *h1p() = (((uint64_t) sn) << 1);
    if (sn == 0) {
        cout << s << endl;
        cout << endl << "Sector 0" << endl << "Possible error in symmetries" << endl;
        throw 1;
    }

    if (sn == common::virtual_sector) { // we never have sector 1 here
        for (unsigned int i = 0; i != common::dimension; ++i) {
#ifdef SMALL_POINT
            if (i & 1)
                ww[sizeof(point) - (i>>1) - 3] |= (8 + pf.buf[i])<<4;
            else
                ww[sizeof(point) - (i>>1) - 3] = 8 + pf.buf[i];
#else
            ww[MAX_IND - i - 1] = (common::small ? 8 : 128) + pf.buf[i];
#endif
        }
    } else {
        t_index *ordering_now = common::orderings_fast[s];
        point_fast degrees = pf.degree();

        // size should be equal to common::dimension in this case
        t_index *pos = ordering_now;

        for (unsigned int i = 0; i != common::dimension; ++i) {
#ifdef SMALL_POINT
            if (!i) ww[sizeof(point) - 3] = 128; // to bit to distinguish it from preferred
#else
            ww[MAX_IND - i - 1] = 1; // to be higher than virtual anyway even for preferred
#endif
            for (unsigned int j = 0; j != common::dimension; ++j) {
#ifdef SMALL_POINT
                if (*pos++) {
                    if (i & 1)
                        ww[sizeof(point) - (i>>1) - 4] += degrees.buf[j]<<4;
                    else
                        ww[sizeof(point) - (i>>1) - 3] += degrees.buf[j];
                }
#else
                if (*pos++) ww[MAX_IND - i - 1] += (degrees.buf[j]);
#endif
            }
        }
        if (preferred_fast[sn].find(pf) == preferred_fast[sn].end()) {
            *h1p() |= ((unsigned short) 1);
        }
    }
}

point::point(const point &p, const point_fast& v, SECTOR ssector, bool not_preferred) {
#ifndef SMALL_POINT
    reinterpret_cast<uint64_t *>(this)[2] = reinterpret_cast<const uint64_t *>(p.ww)[2];
#endif
    reinterpret_cast<uint64_t *>(this)[1] = reinterpret_cast<const uint64_t *>(p.ww)[1];
    reinterpret_cast<uint64_t *>(this)[0] = reinterpret_cast<const uint64_t *>(p.ww)[0];

    t_index *ordering_now = common::orderings_fast[ssector];
    SECTOR bit = 1<<(common::dimension-1);
    t_index *pos = ordering_now;
    for (unsigned int j = 0; j != common::dimension; ++j) {
        if (v.buf[j]) {
            t_index shift = (bit & ssector) ? v.buf[j] : -v.buf[j];
            for (unsigned int i = 0; i != common::dimension; ++i) {
#ifdef SMALL_POINT
                if (*pos) {
                    if (i & 1)
                        ww[sizeof(point) - (i>>1) - 4] += shift<<4;
                    else
                        ww[sizeof(point) - (i>>1) - 3] += shift;
                }
#else
                if (*pos) ww[MAX_IND - i - 1] += shift;
#endif
                pos += common::dimension;
            }
            pos -= common::dimension*common::dimension;
        }
        bit >>= 1;
        ++pos;
    }

    *h1p() &= ~((unsigned short) 1u);
    if (not_preferred) {
        *h1p() |= ((unsigned short) 1);
    }
}


point::point(char *buf) {
    char *str = (char *) this;
    for (unsigned int i = 0; i != sizeof(point); ++i) {
        str[i] = (buf[i] << 4) ^ (buf[i + sizeof(point)] & 15);
    }
}

void point::safe_string(char *buf) const {
    char *str = (char *) this;
    for (unsigned int i = 0; i != sizeof(point); ++i) {
        buf[i] = (str[i] >> 4) | 64;
        buf[i + sizeof(point)] = (str[i] & 15) | 64;
    }
}


string point::number() const {
    stringstream ss(stringstream::out);
    ss << h1();
    unsigned short sn = this->s_number();
    for (int i = 0; i != common::dimension; ++i) {
#ifdef SMALL_POINT
        if ((sn == 1) || (sn == common::virtual_sector)) {
            if (i & 1)
                ss << std::setfill('0') << std::setw(3) << ((int) (((unsigned char *) ww)[sizeof(point) - (i>>1) - 3]&15) - 1 + 128 - 8);
            else
                ss << std::setfill('0') << std::setw(3) << ((int) (((unsigned char *) ww)[sizeof(point) - (i>>1) - 3]>>4) - 1 + 128 - 8);
        }
        else {
            if (!i)
                ss << std::setfill('0') << std::setw(3) << ((int) ((unsigned char *) ww)[sizeof(point) - 3]&127);
            else if (i & 1)
                ss << std::setfill('0') << std::setw(3) << ((int) ((unsigned char *) ww)[sizeof(point) - (i>>1) - 4]>>4);
            else
                ss << std::setfill('0') << std::setw(3) << ((int) ((unsigned char *) ww)[sizeof(point) - (i>>1) - 3]&15);
        }
#else
        if (common::small && ((sn == 1) || (sn == common::virtual_sector)))
            ss << std::setfill('0') << std::setw(3) << ((int) ((unsigned char *) ww)[MAX_IND - i - 1] - 1 + 128 - 8);
        else
            ss << std::setfill('0') << std::setw(3) << ((int) ((unsigned char *) ww)[MAX_IND - i - 1] - 1);
#endif
    }
    return ss.str();
}

bool point::is_zero() const {
#ifndef SMALL_POINT
    return  (((uint64_t * ) this)[2] == 0) &&
            (((uint64_t * ) this)[1] == 0) &&
            (((uint64_t * ) this)[0] == 0);
#else
    return (((uint64_t * ) this)[1] == 0) &&
        (((uint64_t * ) this)[0] == 0);
#endif
}

/**
 * Update output string stream, basically printing the point
 * @param out output ostream
 * @param p point to be printed
 * @return reference to updated output stream
 */
ostream &operator<<(ostream &out, const point &p) {
    out << (point::print_g ? "G[" : "{") << common::global_pn << ",{";
    if (p.virt()) {
        out << p.s_number() << ",";
        out << ((((virt_t * )(p.ww + sizeof(point) - 7))[0]));
    } else {
        unsigned int it = 0;
        vector<t_index> v = p.get_vector();
        out << (int) v[it];
        for (it++; (it != v.size()); it++) out << "," << (int) (v[it]);
    }
    out << "}" << (point::print_g ? "]" : "}");
    return out;
}


vector<t_index> point::get_vector() const {
    unsigned short sn = s_number();
    vector<t_index> result;
    if ((sn == 1) || (sn == common::virtual_sector)) {
        for (int i = 0; i != common::dimension; i++) {
#ifdef SMALL_POINT
            if (i & 1)
                result.push_back((t_index)(ww[sizeof(point) - (i>>1) - 3]&15) - 8);
            else
                result.push_back((t_index)(ww[sizeof(point) - (i>>1) - 3]>>4) - 8);
#else
            result.push_back((t_index)(ww[MAX_IND - i - 1] - (common::small ? 8 : 128)));
#endif
        }
    } else {
        vector<vector<t_index> > &iordering = common::iorderings.find(sn)->second;
        vector<t_index> ssector = common::ssectors[sn];
        int l = iordering.size();
        for (int i = 0; i != l; ++i) {
            t_index z = 0;
            if (!virt()) {
                for (int j = 0; j != l; ++j) {
#ifdef SMALL_POINT
                    if (!j) {
                        if (iordering[i][j] == 1) z += (ww[sizeof(point) - 3]&127);
                        else if (iordering[i][j] == -1) z -= (ww[sizeof(point) - 3]&127);
                    }
                    else if (j&1) {
                        if (iordering[i][j] == 1) z += (ww[sizeof(point) - (j>>1) - 4]>>4);
                        else if (iordering[i][j] == -1) z -= (ww[sizeof(point) - (j>>1) - 4]>>4);
                    }
                    else {
                        if (iordering[i][j] == 1) z += (ww[sizeof(point) - (j>>1) - 3]&15);
                        else if (iordering[i][j] == -1) z -= (ww[sizeof(point) - (j>>1) - 3]&15);
                    }
#else
                    if (iordering[i][j] == 1) z += (ww[MAX_IND - j - 1] - 1);
                    else if (iordering[i][j] == -1) z -= (ww[MAX_IND - j - 1] - 1);
#endif
                }
            }
            if (ssector[i] == 1) z++; else z = -z;
            result.push_back(z);
        }
    }
    return result;
}


set<point_fast> level_points_fast(point_fast s, const unsigned int pos, const unsigned int neg) {
    if ((pos == 0) && (neg == 0)) {
        set<point_fast> r;
        r.insert(s);
        return (r);
    }
    if (neg > 0) {
        set<point_fast> old = level_points_fast(s, pos, neg - 1);
        set<point_fast> r;
        for (const auto & p : old) {
            for (unsigned int i = 0; i < common::dimension; ++i) {
                if (p.buf[i] <= 0) {
                    point_fast p2 = p;
                    p2.buf[i]--;
                    r.insert(p2);
                }
            }
        }
        return r;
    }
    set<point_fast> old = level_points_fast(s, pos - 1, neg);
    set<point_fast> r;
    for (const auto & p : old) {
        for (unsigned int i = 0; i < common::dimension; ++i) {
            if (p.buf[i] > 0) {
                point_fast p2 = p;
                p2.buf[i]++;
                r.insert(p2);
            }
        }
    }
    return r;
}
