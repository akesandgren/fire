/** @file mpi_run_single.cpp
 * @author Fedor Chukharev
 *
 * This file is a part of the FIRE package.
 *
 * Used to launch and control result of work of many instances of FIRE6p and have them run in parallel.
*/
#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <limits.h>
#include <dirent.h>

/**
 * MPI tag for command message.
 */
#define COMMAND 0
/**
 * MPI tag for message with data.
 */
#define DATA 1
/**
 * MPI tag for message with results of work.
 */
#define RESULT 2
/**
 * Default string buffer size,
 */
#define STR_SIZE 256
/**
 * Maximum index of a prime from primes.cpp
 */
#define MAX_P_INDEX 127

/**
 * Type of command send from master process.
 */
enum command_type {
    FINISH, EVAL
};

/**
 * State of FIRE6p result table.
 */
enum table_state {
    NO_TABLE = -1,
    NONEXISTENT = 0,
    PROCESSING = 1,
    CREATED = 2
};


#ifdef WITH_DEBUG

#include <execinfo.h>
#include <signal.h>

char binary[256];

void handler(int sig) {
    void *array[32];
    char **messages = (char **) NULL;
    size_t size;

    // get void*'s for all entries on the stack
    size = backtrace(array, 32);

    // print out all the frames to stderr
    fprintf(stderr, "Error: signal %d:\n", sig);

    printf("Quick backtrace summary\n");
    backtrace_symbols_fd(array, size, STDERR_FILENO);

    printf("Detailed backtrace summary\n");
    messages = backtrace_symbols(array, size);
    /* skip first stack frame (points here) */
    printf("[bt] Execution path:\n");
    for (unsigned int i = 1; i < size; ++i) {
        printf("[bt] #%d %s\n", i, messages[i]);

        char syscom[256];

        sprintf(syscom, "addr2line %p -f -p -i -e %s", array[i], binary);
        if (system(syscom)) printf("Install addr2line for more details next time");
    }
    exit(2);
}
#endif

/**
 * Path to FIRE6p executable.
 */
char EXEC_PATH[128];

/**
 * Default path to problem.
 */
char ARG_PATH[STR_SIZE] = "examples/box";

/**
 * Get next set of arguments for next FIRE6p invocation.
 * @param tables_state matrix of table states
 * @param tables_off resulting offset from the matrix start
 * @param tables_off_min minimal offset to start checking from
 * @param tables_am length of the matrix
 */
void get_arg(int *tables_state, int *tables_off, int *tables_off_min, int tables_am) {
    while (*tables_off_min != tables_am) {
        if (tables_state[*tables_off_min] == CREATED) {
            // going through created tables
            ++*tables_off_min;
        } else break; // it might be non-existent or processing, need to recheck later in case of processing
    }

    for (int i = *tables_off_min; i != tables_am; i++) {
        if (tables_state[i] == NONEXISTENT) {
            *tables_off = i;
            return;
        }
    }
    *tables_off = NO_TABLE;
}

/**
 * Self-made recursive directory removal function.
 * @param dirname full path
 * @return successfullness
 */
int removedirectoryrecursively(const char *dirname) {
    DIR *dir;
    struct dirent *entry;
    char path[PATH_MAX];
    if (path == NULL) {
        fprintf(stderr, "Out of memory error\n");
        return 0;
    }
    dir = opendir(dirname);
    if (dir == NULL) {
        return 0;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
            snprintf(path, (size_t) PATH_MAX, "%s/%s", dirname, entry->d_name);
            if (entry->d_type == DT_DIR) {
                removedirectoryrecursively(path);
            }
            printf("Deleting: %s\n", path);
            remove(path);
        }

    }
    closedir(dir);
    printf("Deleting: %s\n", dirname);
    remove(dirname);
    return 1;
}

/**
 * Entry point for mpi_run_single.cpp.
 * @param argc number of arguments
 * @param argv array of arguments
 * @return successfullness
 */
int main(int argc, char *argv[]) {
    int NET_SIZE, RANK, tmp;
    char proc_name[STR_SIZE];
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
    MPI_Comm_size(MPI_COMM_WORLD, &NET_SIZE);
    MPI_Get_processor_name(proc_name, &tmp);
    if (argc < 5) {
        printf("Incorrect syntax!\n");
        MPI_Finalize();
        return 0;
    }

    // config, p, dmin, dmax, ?lim
    // or
    // config, p, dmin, dmax, xmin, xmax, ?lim

    strncpy(EXEC_PATH, argv[0], 128);
    EXEC_PATH[strlen(EXEC_PATH) - 4] = 'p';
    EXEC_PATH[strlen(EXEC_PATH) - 3] = '\0';

    if (strcmp("-c",argv[1])) {
        printf("Syntax is -c config options!\n");
        MPI_Finalize();
        return 0;
    }

    strncpy(ARG_PATH, argv[2], STR_SIZE);
    if (RANK == 0) {
        if (NET_SIZE < 2) {
            printf("At least 2 processes are needed!\n");
            MPI_Finalize();
            return 0;
        }
        struct timespec start, end;
        clock_gettime(CLOCK_MONOTONIC, &start);
        printf("Total %d worker-processes!\n\n", NET_SIZE - 1);

        int p_index = strtol(argv[3], NULL, 10);
        if (p_index < 0 || p_index > MAX_P_INDEX) {
            printf("Invalid prime number index - resetting it to 1...\n");
            p_index = 1;
        }
        int P_INDEX = p_index;
        int d_start, d_end;
        int x_start, x_end;
        d_start = strtol(argv[4], NULL, 10);
        d_end = strtol(argv[5], NULL, 10);
        int lim = 0; // maximal number of runs, 0 will lead to infinity

        if (argc <= 7) {
            x_start = 1;
            x_end = 1;
            if (argc == 7) {
                lim = strtol(argv[6], NULL, 10);
            }
        } else {
            x_start = strtol(argv[6], NULL, 10);
            x_end = strtol(argv[7], NULL, 10);
            if (argc == 9) {
                lim = strtol(argv[8], NULL, 10);
            }
        }
        if (lim != 0) {
            printf("Limit of child runs: %d\n", lim);
        }

        if (d_end < d_start) {
            printf("Upper bound d value is lower than lower bound - swapping them...\n");
            tmp = d_end;
            d_end = d_start;
            d_start = tmp;
        }
        if (x_end < x_start) {
            printf("Upper bound x value is lower than lower bound - swapping them...\n");
            tmp = x_end;
            x_end = x_start;
            x_start = tmp;
        }
        printf("Current vals:\n\targument = %s\n\tp_index = %d\n\td_start = %d\n\td_end = %d\n\tx_start = %d\n\tx_end = %d\n",
               ARG_PATH, p_index, d_start, d_end, x_start, x_end);
        int i, tables_off = 0;
        int tables_off_min = 0; // all the lower tables are done
        const int D_RANGE = d_end - d_start + 1;
        const int X_RANGE = x_end - x_start + 1;
        int *answers[NET_SIZE];
        for (i = 0; i < NET_SIZE; i++) {
            answers[i] = (int *) malloc(4 * sizeof(int)); // [0] - d_value, [1] - p_index, [2] - p_index, [3] - status
        }
        int *tables_state = (int *) calloc(X_RANGE * D_RANGE * P_INDEX, sizeof(int));

        FILE *file;
        char filename[128];
        sprintf(filename, "%s.config", ARG_PATH);
        file = fopen(filename, "r");
        if (file == 0) {
            printf("No %s, exiting!\n", filename);
            return 1;
        }
        char load_string[1000] = "none";

        char output_buf[128];
        output_buf[0] = '\0';
        while (fgets(load_string, 1000, file)) {
            if (!strncmp(load_string, "#folder", 7)) {
                int pos = 7;
                while (load_string[pos] == ' ') pos++;
                strcpy(output_buf, load_string + pos);
                char *temp = output_buf;
                while (*temp) {
                    if (*temp == '\n') {
                        *temp = '\0';
                        break;
                    }
                    ++temp;
                }
            }
            if (!strncmp(load_string, "#output", 7)) {
                int pos = 7;
                while (load_string[pos] == ' ') pos++;
                if (load_string[pos] == '/') {
                    output_buf[0] = '\0'; // ignoring folder settings for absolute paths
                }
                strcpy(output_buf + strlen(output_buf), load_string + pos);

                char *temp = output_buf;
                while (*temp) {
                    if (*temp == '\n') {
                        *temp = '\0';
                        break;
                    }
                    ++temp;
                }
            }
        }
        fclose(file);

        printf("Requested tables output: %s\n", output_buf);

        if (!strncmp(output_buf + strlen(output_buf) - 7, ".tables", 7)) {
            output_buf[strlen(output_buf) - 7] = '\0';
        }

        // let's check for existing tables from the previous run
        for (i = 1; i != P_INDEX + 1; ++i) {
            for (int k = x_start; k != x_start + X_RANGE; ++k) {
                for (int j = d_start; j != d_start + D_RANGE; ++j) {
                    char buf[128];
                    if (argc <= 7) {// no x-variable
                        sprintf(buf, "%s-%d-%d.tables", output_buf, j, i);
                    } else {
                        sprintf(buf, "%s-%d-%d-%d.tables", output_buf, j, k, i);
                    }
                    file = fopen(buf, "r");
                    if (file != NULL) {
                        fgetc(file);
                        if (!feof(file)) {
                            printf("Tables already exist: %s\n", buf);
                        } else {
                            printf("Tables are reserved: %s\n", buf);
                        }
                        tables_state[((k - x_start) * D_RANGE + (j - d_start)) * P_INDEX + (i - 1)] = CREATED;
                        fclose(file);
                    }
                }
            }
        }

        int args[3];
        //'' = {d_start};
        enum command_type command = EVAL;
        MPI_Request *reqs = (MPI_Request *) malloc(NET_SIZE * sizeof(MPI_Request));
        MPI_Status *status = (MPI_Status *) malloc(NET_SIZE * sizeof(MPI_Status));
        int *lims = (int *) malloc(NET_SIZE * sizeof(int));
        for (i = 1; i < NET_SIZE; ++i) {
            lims[i] = lim;
            get_arg(tables_state, &tables_off, &tables_off_min, X_RANGE * D_RANGE * P_INDEX);
            if (tables_off == -1) break; // nothing more to submit

            args[0] = x_start + (tables_off / (P_INDEX * D_RANGE));
            args[1] = d_start + ((tables_off / P_INDEX) % D_RANGE);
            args[2] = 1 + tables_off % P_INDEX;

            char buf[256];
            if (argc <= 7) // no x-variable
                sprintf(buf, "touch %s-%d-%d.tables", output_buf, args[1], args[2]);
            else
                sprintf(buf, "touch %s-%d-%d-%d.tables", output_buf, args[1], args[0], args[2]);
            if (system(buf)) {
                printf("Could not reserve tables file");
                printf("%s\n", buf);
                throw 1;
            }

            MPI_Send(&command, 1, MPI_INT, i, COMMAND, MPI_COMM_WORLD);
            MPI_Send(args, 3, MPI_INT, i, DATA, MPI_COMM_WORLD);
            MPI_Irecv(answers[i], 4, MPI_INT, i, RESULT, MPI_COMM_WORLD, &reqs[i]);
            tables_state[tables_off] = PROCESSING;
        }

        int new_NET_SIZE = i;
        if (i < NET_SIZE) {
            printf("Too many workers - shutting them down...\n");
            command = FINISH;
            for (; i < NET_SIZE; i++) {
                MPI_Send(&command, 1, MPI_INT, i, COMMAND, MPI_COMM_WORLD);
            }
        }
        NET_SIZE = new_NET_SIZE;
        reqs[0] = MPI_REQUEST_NULL;
        int res_i;

        MPI_Waitany(NET_SIZE, reqs, &res_i, status);

        while (res_i != MPI_UNDEFINED) {
            if (!answers[res_i][3]) {
                tables_state[((answers[res_i][0] - x_start) * D_RANGE + (answers[res_i][1] - d_start)) * P_INDEX +
                             (answers[res_i][2] - 1)] = CREATED;
            } else {
                tables_off_min = ((answers[res_i][0] - x_start) * D_RANGE + (answers[res_i][1] - d_start)) * P_INDEX +
                                 (answers[res_i][2] - 1);
                tables_state[((answers[res_i][0] - x_start) * D_RANGE + (answers[res_i][1] - d_start)) * P_INDEX +
                             (answers[res_i][2] - 1)] = NONEXISTENT;
            }

            --lims[res_i];

            if (lims[res_i] == 0) {
                // this worker did all it could;
                // let's stop it and continue
                printf("Stopping worker %d\n", res_i);
                command = FINISH;
                MPI_Send(&command, 1, MPI_INT, res_i, COMMAND, MPI_COMM_WORLD);
                MPI_Waitany(NET_SIZE, reqs, &res_i, MPI_STATUS_IGNORE);
                continue;
            }

new_table:
            get_arg(tables_state, &tables_off, &tables_off_min, X_RANGE * D_RANGE * P_INDEX);
            if (tables_off != NO_TABLE) { // our limit of tasks
                command = EVAL;
                args[0] = x_start + (tables_off / (P_INDEX * D_RANGE));
                args[1] = d_start + ((tables_off / P_INDEX) % D_RANGE);
                args[2] = 1 + tables_off % P_INDEX;

                char buf[256];
                if (argc <= 7) {// no x-variable
                    sprintf(buf, "%s-%d-%d.tables", output_buf, args[1], args[2]);
                } else {
                    sprintf(buf, "%s-%d-%d-%d.tables", output_buf, args[1], args[0], args[2]);
                }

                // checking if the file appeared
                file = fopen(buf, "r");
                if (file != NULL) {
                    fgetc(file);
                    if (!feof(file)) {
                        printf("Tables already exist: %s\n", buf);
                    } else {
                        printf("Tables are reserved: %s\n", buf);
                    }
                    tables_state[tables_off] = CREATED;
                    fclose(file);
                    goto new_table; // yes, one should not use goto
                }


                if (argc <= 7) { // no x-variable
                    sprintf(buf, "touch %s-%d-%d.tables", output_buf, args[1], args[2]);
                } else {
                    sprintf(buf, "touch %s-%d-%d-%d.tables", output_buf, args[1], args[0], args[2]);
                }
                if (system(buf)) {
                    printf("Could not reserve tables file");
                    printf("%s\n", buf);
                    throw 1;
                }

                MPI_Send(&command, 1, MPI_INT, res_i, COMMAND, MPI_COMM_WORLD);
                MPI_Send(args, 3, MPI_INT, res_i, DATA, MPI_COMM_WORLD);
                MPI_Irecv(answers[res_i], 4, MPI_INT, res_i, RESULT, MPI_COMM_WORLD, &reqs[res_i]);
                tables_state[tables_off] = PROCESSING;
            } else {
                command = FINISH;
                printf("Stopping worker %d (no tasks left)\n", res_i);
                MPI_Send(&command, 1, MPI_INT, res_i, COMMAND, MPI_COMM_WORLD);
            }
            MPI_Waitany(NET_SIZE, reqs, &res_i, MPI_STATUS_IGNORE);
        }
        printf("Finished all jobs\n");
        clock_gettime(CLOCK_MONOTONIC, &end);
        printf("Your calculations took %.6lf seconds to run.\n",
               (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1E9);

        get_arg(tables_state, &tables_off, &tables_off_min, X_RANGE * D_RANGE * P_INDEX);
        if (tables_off != NO_TABLE) {
            printf("Some tables where not created\n");
        }


        /*Worker part below*/
    } else {
#ifdef WITH_DEBUG
        strcpy(binary, argv[0]);
        signal(SIGSEGV, handler);  //install handler to print errors
        signal(SIGBUS, handler);  //signal 7 that we do not understand
#endif
        FILE *file;
        char filename[128];
        sprintf(filename, "%s.config", ARG_PATH);
        file = fopen(filename, "r");
        if (file == 0) {
            printf("No %s, exiting!\n", filename);
            return 1;
        }
        char load_string[1000] = "none";

        char output_buf[128];
        char database[128] = "temp/db/";
        output_buf[0] = '\0';
        while (fgets(load_string, 1000, file)) {
            if (!strncmp(load_string, "#folder", 7)) {
                int pos = 7;
                while (load_string[pos] == ' ') pos++;
                strcpy(output_buf, load_string + pos);
                char *temp = output_buf;
                while (*temp) {
                    if (*temp == '\n') {
                        *temp = '\0';
                        break;
                    }
                    ++temp;
                }
            }
            if (!strncmp(load_string, "#database", 9)) {
                int pos = 9;
                while (load_string[pos] == ' ') pos++;
                strcpy(database, load_string + pos);
                char *temp = database;
                while (*temp) {
                    if (*temp == '\n') {
                        *temp = '\0';
                        break;
                    }
                    ++temp;
                }
                if (database[strlen(database) - 1] != '/') {
                    database[strlen(database) + 1] = '\0';
                    database[strlen(database)] = '/';
                }
            }
            if (!strncmp(load_string, "#output", 7)) {
                int pos = 7;
                while (load_string[pos] == ' ') pos++;
                if (load_string[pos] == '/') output_buf[0] = '\0'; // ignoring folder settings for absolute paths
                strcpy(output_buf + strlen(output_buf), load_string + pos);

                char *temp = output_buf;
                while (*temp) {
                    if (*temp == '\n') {
                        *temp = '\0';
                        break;
                    }
                    ++temp;
                }
            }
        }
        fclose(file);

        if (!strncmp(output_buf + strlen(output_buf) - 7, ".tables", 7)) {
            output_buf[strlen(output_buf) - 7] = '\0';
        }
        enum command_type command;
        const int MASTER = 0;
        int result[4];
        int args[3];

        MPI_Recv(&command, 1, MPI_INT, MASTER, COMMAND, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        while (command != FINISH) {
            MPI_Recv(args, 3, MPI_INT, MASTER, DATA, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            result[0] = args[0]; // x value
            result[1] = args[1]; // d value
            result[2] = args[2]; // p_index

            char *run_args[16];
            char par1[16] = "-variables";
            char par2[32];
            if (argc <= 7) {
                sprintf(par2, "%d-%d", args[1], args[2]);
            } else {
                sprintf(par2, "%d-%d-%d", args[1], args[0], args[2]);
            }
            char par3[8] = "-c";
            char par4[16] = "-parallel";
            char par5[8] = "-silent";
            if (NET_SIZE == 2) par5[0] = '\0';
            run_args[0] = EXEC_PATH;
            run_args[1] = par1;
            run_args[2] = par2;
            run_args[3] = par3;
            run_args[4] = ARG_PATH;
            run_args[5] = par4;
            run_args[6] = par5;
            run_args[7] = (char *) 0;

            for (int i = 0; run_args[i] != (char *) 0; ++i) printf("%s ",run_args[i]);
            printf("\n");

            printf("STARTING A TASK - %d-TH PROCESS\n", RANK);
            pid_t pid = fork();

            if (pid == -1) {
                printf("Error on fork\n");
                throw 1;
            } else if (pid > 0) {
                // that's the parent process
                int status;
                //pid_t return_value =
                waitpid(pid, &status, 0);
                if (WIFSIGNALED(status)) {
                    printf("TASK %d EXITED AFTER SIGNAL %d: %s\n", RANK, WTERMSIG(status), strsignal(WTERMSIG(status)));
                    result[3] = 1;
                } else if (!WIFEXITED(status)) {
                    printf("TASK %d EXITED WITH ERROR STATUS %d\n", RANK, status);
                    result[3] = 1;
                } else {
                    result[3] = WEXITSTATUS(status);
                    if (result[3]) {
                        printf("TASK %d ENDED WITH RETURN VALUE %d\n", RANK, WEXITSTATUS(status));
                    } else
                        printf("TASK %d ENDED SUCCESSFULLY\n", RANK);
                }

                char buf[256];
                // check if tables are empty

                if (argc <= 7) {// no x-variable
                    sprintf(buf, "%s-%d-%d.tables", output_buf, args[1], args[2]);
                } else {
                    sprintf(buf, "%s-%d-%d-%d.tables", output_buf, args[1], args[0], args[2]);
                }

                // checking if the file appeared
                FILE *file;
                file = fopen(buf, "r");
                if (file != NULL) {
                    fgetc(file);
                    if (feof(file)) {
                        printf("Reserved tables not created, deleting: %s\n", buf);
                        char buf2[256];
                        sprintf(buf2, "rm %s", buf);
                        if (system(buf2)) {
                            printf("Could not delete temporary tables");
                            throw 1;
                        }
                    }
                    fclose(file);
                }

                // now we need to remove temporary database after the child
                char hostname[64];
                gethostname(hostname, 64);

                sprintf(buf, "%s%s-%d", database, hostname, pid);
                printf("Removing %s\n", buf);
                removedirectoryrecursively(buf);
                printf("Removed %s\n", buf);
            } else {
                // that's the child process
                execv(EXEC_PATH, run_args);
                // if exec is succesfull, we do not get here
                perror("execve");
                throw 1;
            }
            MPI_Send(result, 4, MPI_INT, MASTER, RESULT, MPI_COMM_WORLD);
            MPI_Recv(&command, 1, MPI_INT, MASTER, COMMAND, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
    }
    MPI_Finalize();
    return 0;
}
