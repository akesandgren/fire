/** @file equation.cpp
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package.
 */

#include "equation.h"

//initialization of static class members
map<vector<t_index>, point> equation::initial;

pthread_mutex_t equation::f_submit_mutex[MAX_THREADS];
pthread_mutex_t equation::f_receive_mutex[MAX_THREADS];
sem_t *equation::f_submit_sem[MAX_THREADS];
sem_t *equation::f_receive_sem[MAX_THREADS];
list<pair<int, pair<point, string> > > equation::f_jobs[MAX_THREADS];
bool equation::f_stop = false;
list<pair<point, string> > equation::f_result[MAX_THREADS];
pthread_t equation::f_threads[MAX_THREADS];


// get monoms from a point (Feynman integral). database access used
vector<point> p_get_monoms(const point &p, unsigned short fixed_database_sector) {
    vector<point> result;
    unsigned short dsector = (fixed_database_sector == 0) ? p.s_number() : fixed_database_sector;

    size_t size;
    char *res = kcdbget(common::points[dsector], (const char *) &p, sizeof(point), &size);
    if (res == nullptr) {
        if (kcdbecode(common::points[dsector]) != KCENOREC) {
            cout << p.s_number() << p << ((string) kcdbemsg(common::points[dsector])) << endl;
            throw 1;
        }
        return result;
    }

    unsigned short len = ((unsigned short *) res)[0];
    result.reserve(len);
    if (common::small) {
        unsigned char *poss = (unsigned char *) res + 3 - 1;
        unsigned char *posp = (unsigned char *) res + 3 + (len >> 2) + ((len & 3) ? 1 : 0); // the number of bytes needed to have 2 bit per point
        unsigned short i = 8; // cycles through the internal 4-loop
        for (unsigned int j = 0; j != len; ++j) {
            if (i == 8) {
                ++poss;
                i = 0;
            }
            unsigned char type = ((*poss) >> i) & 3;
#ifdef SMALL_POINT
            alignas(16) unsigned char pbuf[sizeof(point)];
            reinterpret_cast<uint64_t *>(pbuf)[0] = 0;
            reinterpret_cast<uint64_t *>(pbuf)[1] = 0;
#else
            alignas(8) unsigned char pbuf[sizeof(point)];
            reinterpret_cast<uint64_t *>(pbuf)[0] = 0;
            reinterpret_cast<uint64_t *>(pbuf)[1] = 0;
            reinterpret_cast<uint64_t *>(pbuf)[2] = 0;
#endif

            if (type == 3) {
                // point in a lower sector
#ifdef SMALL_POINT
                unsigned short size = (2 + ((common::dimension + 1) >> 1) + ((common::dimension + 1) & 1));
                memcpy(pbuf + sizeof(point) - size,posp, size);
                posp += size;
#else
                *(unsigned short *) (pbuf + 22) = *(unsigned short *) (posp + (common::dimension >> 1) + (common::dimension & 1));
                for (unsigned short k = 0; k != (common::dimension >> 1) + (common::dimension & 1); ++k) {
                    pbuf[MAX_IND - (k << 1) - 1] = posp[k] & 15;  // decrypting one out of two
                    pbuf[MAX_IND - (k << 1) - 2] = posp[k] >> 4;
                }
                posp += (2 + (common::dimension >> 1) + (common::dimension & 1));
#endif
            } else if (type == 2) {
                // virtual
                *(unsigned short *) (pbuf + sizeof(point) - 2) = ((p.s_number() << 1)); // virtual points have 0 at the preffered bit
                *(uint32_t *) (pbuf + sizeof(point) - 7) = *(uint32_t *) posp; // virtual number
                *(unsigned char *) (pbuf + sizeof(point) - 3) = 0; // virtual indication
                posp += 4;
            } else { // 0 or 1
                // points in this sector
#ifdef SMALL_POINT
                unsigned short size = ((common::dimension + 1) >> 1) + ((common::dimension + 1) & 1);
                memcpy(pbuf + sizeof(point) - size - 2,posp, size);
                posp += size;
#else
                for (unsigned short k = 0; k != (common::dimension >> 1) + (common::dimension & 1); ++k) {
                    pbuf[MAX_IND - (k << 1) - 1] = posp[k] & 15;  // decrypting one out of two
                    pbuf[MAX_IND - (k << 1) - 2] = posp[k] >> 4;
                }
                posp += ((common::dimension >> 1) + (common::dimension & 1));
#endif
                *(unsigned short *) (pbuf + sizeof(point) - 2) = ((p.s_number() << 1) ^ type); // that's the preffered bit
            }
            i += 2;
            result.emplace_back(*reinterpret_cast<point *>(pbuf));
        }
    } else {
        for (unsigned int i = 0; i != len; ++i) {
            result.emplace_back((reinterpret_cast<point *>(res + 3))[i]);
        }
    }

    kcfree(res);
    if ((len != 0) && (p != result.back())) {
        cout << "get_monoms error" << endl;
        cout << p << endl;
        cout << result.size() << endl;
        for (const auto & pnt : result) {
            cout << pnt << endl;
        }
        throw 1;
    }
    return result;
}

bool p_is_empty(const point &p, unsigned short fixed_database_sector) {
    unsigned short dsector = (fixed_database_sector == 0) ? p.s_number() : fixed_database_sector;
    size_t size;
    char *res = kcdbget(common::points[dsector], (const char *) &p, sizeof(point), &size);
    if (res == nullptr) {
        if (kcdbecode(common::points[dsector]) != KCENOREC) {
            cout << p.s_number() << p << ((string) kcdbemsg(common::points[dsector])) << endl;
            throw 1;
        }
        return true;
    }
    unsigned short len = ((unsigned short *) res)[0];
    kcfree(res);
    return len == 0;
}

void p_get(const point &p, vector<pair<point, COEFF> > &terms, unsigned short fixed_database_sector) {
    unsigned short dsector = (fixed_database_sector == 0) ? p.s_number() : fixed_database_sector;
    size_t size;
    char *res = kcdbget(common::points[dsector], (const char *) &p, sizeof(point), &size);
    if (res == nullptr) {
        if (kcdbecode(common::points[dsector]) != KCENOREC) {
            cout << p.s_number() << p << ((string) kcdbemsg(common::points[dsector])) << endl;
            throw 1;
        }
        terms.clear();
        return;
    }

    unsigned short len = ((unsigned short *) res)[0];
    if (len == 0) {
        terms.clear();
        kcfree(res);
        return;
    } // empty point

    terms.reserve(len);  // this is the only difference! I could not make this with templates in c++11 (only 17 I think)

    p_get_internal(p, terms, dsector, len, res);
}

void p_get(const point &p, list<pair<point, COEFF>, ALLOCATOR2> &terms, unsigned short fixed_database_sector) {
    unsigned short dsector = (fixed_database_sector == 0) ? p.s_number() : fixed_database_sector;
    size_t size;
    char *res = kcdbget(common::points[dsector], (const char *) &p, sizeof(point), &size);
    if (res == nullptr) {
        if (kcdbecode(common::points[dsector]) != KCENOREC) {
            cout << p.s_number() << p << ((string) kcdbemsg(common::points[dsector])) << endl;
            throw 1;
        }
        terms.clear();
        return;
    }

    unsigned short len = ((unsigned short *) res)[0];
    if (len == 0) {
        terms.clear();
        kcfree(res);
        return;
    } // empty point

    p_get_internal(p, terms, dsector, len, res);
}

// get monoms and coefficients from a point (Feynman integral). database access used
template<class I>
void p_get_internal(const point &p, I &terms, unsigned short dsector, unsigned short len, char* res) {

    char *pos;
    if (common::small) {
        // we have to calculate the coefficient shift
        unsigned char *poss = (unsigned char *) res + 3 - 1;
        unsigned char *posp = (unsigned char *) res + 3 + (len >> 2) + ((len & 3) ? 1 : 0); // the number of bytes needed to have 2 bit per point
        unsigned short i = 8; // cycles through the internal 4-loop
        for (unsigned int j = 0; j != len; ++j) {
            if (i == 8) {
                ++poss;
                i = 0;
            }
            unsigned char type = ((*poss) >> i) & 3;
            if (type == 3) {
#ifdef SMALL_POINT
                posp += (2 + ((common::dimension + 1) >> 1) + ((common::dimension + 1) & 1));
#else
                posp += (2 + (common::dimension >> 1) + (common::dimension & 1));
#endif
            } else if (type == 2) {
                posp += 4;
            } else { // 0 or 1
#ifdef SMALL_POINT
                posp += (((common::dimension + 1) >> 1) + ((common::dimension + 1) & 1));
#else
                posp += ((common::dimension >> 1) + (common::dimension & 1));
#endif
            }
            i += 2;
        }
        pos = (char *) posp;
    } else {
        pos = res + 3 + len * sizeof(point);
    }

    unsigned char *poss = (unsigned char *) res + 3 - 1; // needed only in small
    unsigned char *posp = (unsigned char *) res + 3 + (len >> 2) + ((len & 3) ? 1 : 0); // the number of bytes needed to have 2 bit per point
    unsigned short i = 8; // cycles through the internal 4-loop

    for (unsigned int j = 0; j != len; ++j) {
        COEFF c;
#ifdef PRIME
        c.n = *(unsigned long long *) pos;
        pos += sizeof(unsigned long long);
#else
        char *end = pos;
        while (*end != '|') ++end;
        *end = '\0';
        c.s = (string) pos;
        pos = end;
        ++pos;
#endif
        if (common::small) {
            if (i == 8) {
                ++poss;
                i = 0;
            }
            unsigned char type = ((*poss) >> i) & 3;
#ifdef SMALL_POINT
            alignas(16) unsigned char pbuf[sizeof(point)];
            reinterpret_cast<uint64_t *>(pbuf)[0] = 0;
            reinterpret_cast<uint64_t *>(pbuf)[1] = 0;
#else
            alignas(8) unsigned char pbuf[sizeof(point)];
            reinterpret_cast<uint64_t *>(pbuf)[0] = 0;
            reinterpret_cast<uint64_t *>(pbuf)[1] = 0;
            reinterpret_cast<uint64_t *>(pbuf)[2] = 0;
#endif

            if (type == 3) {
                // point in a lower sector
#ifdef SMALL_POINT
                unsigned short size = (2 + ((common::dimension + 1) >> 1) + ((common::dimension + 1) & 1));
                memcpy(pbuf + sizeof(point) - size,posp, size);
                posp += size;
#else
                *(unsigned short *) (pbuf + 22) = *(unsigned short *) (posp + (common::dimension >> 1) + (common::dimension & 1));
                for (unsigned short k = 0; k != (common::dimension >> 1) + (common::dimension & 1); ++k) {
                    pbuf[MAX_IND - (k << 1) - 1] = posp[k] & 15;  // decrypting one out of two
                    pbuf[MAX_IND - (k << 1) - 2] = posp[k] >> 4;
                }
                posp += (2 + (common::dimension >> 1) + (common::dimension & 1));
#endif
            } else if (type == 2) {
                // virtual
                *(unsigned short *) (pbuf + sizeof(point) - 2) = ((p.s_number() << 1)); // virtual points have 0 at the preffered bit
                *(uint32_t *) (pbuf + sizeof(point) - 7) = *(uint32_t *) posp; // virtual number
                *(unsigned char *) (pbuf + sizeof(point) - 3) = 0; // virtual indication
                posp += 4;
            } else { // 0 or 1
                // points in this sector
#ifdef SMALL_POINT
                unsigned short size = ((common::dimension + 1) >> 1) + ((common::dimension + 1) & 1);
                memcpy(pbuf + sizeof(point) - size - 2,posp, size);
                posp += size;
#else
                unsigned char* posfrom = posp;
                unsigned char* posto = pbuf + MAX_IND - 1;
                posp += ((common::dimension >> 1) + (common::dimension & 1));
                while (posfrom != posp) {
                    *(posto--) = *posfrom & 15;  // decrypting one out of two
                    *(posto--) = *posfrom >> 4;
                    ++posfrom;
                }
#endif
                *(unsigned short *) (pbuf + sizeof(point) - 2) = ((p.s_number() << 1) ^ type); // that's the preffered bit
            }
            i += 2;
            terms.emplace_back(*reinterpret_cast<point *>(pbuf), c);
        } else {
            terms.emplace_back(reinterpret_cast<point *>(res + 3)[j], c);
        }
    }

    kcfree(res);
    if ((len != 0) && (p != terms.back().first)) {
        cout << "p_get error" << endl;
        cout << p << endl;
        cout << terms.size() << endl;
        for (const auto & term : terms) {
            cout << term.first << endl;
        }
        throw 1;
    }
}


/** @brief call back function for an existing record, that is to be modified
 * @param kbuf the buffer of the record key
 * @param ksiz key size
 * @param vbuf the buffer of the visited record value
 * @param vsiz value size
 * @param sp size of new entry written
 * @param opq buffer passed with the new entry and extra data
 * @return the start of the buffer to write into the database
 */
const char *visitfullset(const char *kbuf, size_t ksiz, const char *vbuf, size_t vsiz, size_t *sp, void *opq) {
    const point p = *reinterpret_cast<const point *>(kbuf);
    char *buf = (char *) opq;
    *sp = ((uint32_t *) buf)[0];
    unsigned short n = ((unsigned short *) buf)[2];
    unsigned char level = ((unsigned char *) buf)[6];
    unsigned short len = 0;
    unsigned char old_level = 0;
    const char *res = vbuf;

    len = ((unsigned short *) res)[0];
    old_level = ((unsigned char *) res)[2];  // third byte

    if ((len != 0) && (n == 0)) {
        //this means that we have a rule in a lower sector and now it is being marked as needed
        //we should not change the table in this case... but only check the level it is needed up to
        return KCVISNOP;
    }

    if ((old_level == 255) && (n) && (level != 255)) {
        // the point was marked as a master, but now we have a table
        cout << "SOMEHOW " << p << " IS NO LONGER A MASTER!!!" << endl;
        old_level = 254;
    }
    unsigned char new_level = old_level;
    if (level > new_level) new_level = level;
    ((unsigned char *) buf)[6] = new_level;
    return buf + 4;
}


/** @brief call back function for an empty record space, writing anew
 * @param kbuf the buffer of the record key
 * @param ksiz key size
 * @param sp size of new entry written
 * @param opq buffer passed with the new entry and extra data
 * @return the start of the buffer to write into the database
 */
const char *visitemptyset(const char *kbuf, size_t ksiz, size_t *sp, void *opq) {
    *sp = ((uint32_t *) opq)[0];
    return (char *) opq + 4;
}

void p_set(const point &p, const vector<pair<point, COEFF> > &terms, unsigned char level, unsigned short fixed_database_sector) {
    p_set(p, static_cast<unsigned int>(terms.size()), terms.begin(), terms.end(), level, fixed_database_sector);
}

void p_set(const point &p, const list<pair<point, COEFF>, ALLOCATOR2 > &terms, unsigned char level, unsigned short fixed_database_sector) {
    p_set(p, static_cast<unsigned int>(terms.size()), terms.begin(), terms.end(), level, fixed_database_sector);
}

template<class I>
void p_set(const point &p, unsigned int n, I termsB, I termsE, unsigned char level, unsigned short fixed_database_sector) {
    unsigned short dsector = (fixed_database_sector == 0) ? p.s_number() : fixed_database_sector;
    unsigned int string_size = 0;
#ifdef PRIME
    string_size = n * sizeof(unsigned long long); // we will just put our number into the buffer
#else
    for (auto itr = termsB; itr != termsE; ++itr) {
        string_size += (itr->second.s.size() + 1);
    }
#endif
    char *buf = (char *) malloc(7 + n * sizeof(point) + string_size + 1); // starting with 2 bytes for the size of buf itself
    // that's the maximal buffer size; the real size will be actually smaller, but it will be allocated by kc itself

    // the structure of the buffer
    // 1) the size of the buffer after item 1, 4 bytes
    // 2) n, 2 bytes
    // 3) level, 1 byte
    // 4) types of lower points, n/4 bytes in case of small, nothing otherwise
    // 5) points, differs in case of small, 24*n otherwise
    // 6) coeffs, 8*n in case of prime, string_size otherwise
    // 7) the 0 symbol, just in case

    if (!buf) {
        cout<<"Cannot malloc in p_set"<<endl;
        throw 1;
    }

    ((unsigned short *) buf)[2] = n; // bytes 5 and 6
    ((unsigned char *) buf)[6] = level; // byte 7

    unsigned int points_size;
    if (common::small) {
        unsigned char *poss = (unsigned char *) buf + 7 - 1;
        unsigned char *posp = (unsigned char *) buf + 7 + (n >> 2) + ((n & 3) ? 1 : 0); // the number of bytes needed to have 2 bit per point
        unsigned short i = 8; // cycles through the internal 4-loop
        for (auto itr = termsB; itr != termsE; ++itr) {
            if (i == 8) {
                ++poss;
                *poss = 0;
                i = 0;
            }
            if (itr->first.s_number() != p.s_number()) {
                *poss ^= (3) << i; // lower sector point
#ifdef SMALL_POINT
                unsigned short size = (2 + ((common::dimension + 1) >> 1) + ((common::dimension + 1) & 1));
                memcpy(posp,((unsigned char *) &itr->first) + sizeof(point) - size, size);
                posp += size;
#else
                for (unsigned short k = 0; k != (common::dimension >> 1) + (common::dimension & 1); ++k) {
                    posp[k] = ((unsigned char *) &itr->first)[MAX_IND - (k << 1) - 1] ^
                              (((unsigned char *) &itr->first)[MAX_IND - (k << 1) - 2] << 4); // making it compact
                }
                *(unsigned short *) (posp + (common::dimension >> 1) + (common::dimension & 1)) = itr->first.h1();
                posp += (2 + (common::dimension >> 1) + (common::dimension & 1)); // half dimension + space for h1
#endif
            } else if (itr->first.virt()) {
                *poss ^= (2) << i; // virtual point
                *(uint32_t *) posp = *(uint32_t *) (itr->first.ww + sizeof(point) - 7); // that's where the virtual number is stored
                posp += 4; // only storing the virtual number
            } else {
                // standard points in the same sector
                *poss ^= (itr->first.h1() & 1) << i; // the bit indicating its priority, the high bit is 0
#ifdef SMALL_POINT
                unsigned short size = ((common::dimension + 1) >> 1) + ((common::dimension + 1) & 1);
                memcpy(posp,((unsigned char *) &itr->first) + sizeof(point) - size - 2, size);
                posp += size;
#else
                for (unsigned short k = 0; k != (common::dimension >> 1) + (common::dimension & 1); ++k) {
                    posp[k] = ((unsigned char *) &itr->first)[MAX_IND - (k << 1) - 1] ^
                              (((unsigned char *) &itr->first)[MAX_IND - (k << 1) - 2]
                                      << 4);  // similar to lower points but we do not store h1
                }
                posp += ((common::dimension >> 1) + (common::dimension & 1)); // half dimension
#endif
            }
            i += 2;
        }
        points_size = posp - ((unsigned char *) buf + 7);
    } else {
        unsigned short j = 0;
        for (auto itr = termsB; itr != termsE; ++itr) {
            reinterpret_cast<point *>(buf + 7)[j] = itr->first;
            ++j;
        }
        points_size = n * sizeof(point);
    }

    ((uint32_t *) buf)[0] = 3 + points_size + string_size + 1; // bytes 1-4
    if (n) { // just some checks
        auto terms_itr = termsE;
        --terms_itr;
        if (p != terms_itr->first) {
            cout << "p_set error" << endl;
            cout << p << endl;
            cout << n << endl;
            for (auto itr = termsB; itr != termsE; ++itr) { cout << itr->first << endl; }
            throw 1;
        }
    }
    char *pos = buf + 7 + points_size; // preparing a string of coeffs
    for (auto itr = termsB; itr != termsE; ++itr) {
#ifdef PRIME
        *(unsigned long long *) pos = itr->second.n;
        pos += sizeof(unsigned long long);
#else
        strncpy(pos, itr->second.s.c_str(), itr->second.s.size());
        pos += itr->second.s.size();
        *pos = '|';
        ++pos;
#endif
    }
    *pos = '\0'; // do we really need it? we won't be interpreting the contents as a string any longer
    kcdbaccept(common::points[dsector], (const char *) &p, sizeof(point),
               visitfullset,
               visitemptyset,
               buf, true
    );
    free(buf);
}

bool is_lower_in_orbit(const vector<t_index> &lhs, const vector<t_index> &rhs) {
    if (lhs == rhs) return false;
    vector<t_index> s1 = sector(lhs);
    vector<t_index> s2 = sector(rhs);
    if (s1 != s2) {
        unsigned short sn1 = common::sector_numbers_fast[sector_fast(s1)];
        unsigned short sn2 = common::sector_numbers_fast[sector_fast(s2)];
        if (sn1 < sn2) return (true);
        if (sn1 > sn2) return (false);
    }

    t_index *ordering_now = common::orderings_fast[sector_fast(lhs)];
    vector<t_index> d1 = degree(lhs);
    vector<t_index> d2 = degree(rhs);
    unsigned int n = lhs.size();
    for (unsigned int i = 0; i != n; ++i) {
        int pr = 0;
        for (unsigned int j = 0; j != n; ++j) {
            if (ordering_now[i * common::dimension + j] == 1) {
                pr += (d1[j] - d2[j]);
            }
        }
        if (pr < 0) return true;
        if (pr > 0) return false;
    }
    return false;
}


// sort pairs of points and coefficients using only points
bool pair_point_COEFF_smaller(const pair<point, COEFF> &lhs, const pair<point, COEFF> &rhs) {
    return lhs.first < rhs.first;
}


bool point_fast_smaller_in_sector(const point_fast & pf1, const point_fast & pf2, SECTOR s) {
    if (pf1 == pf2) return false;
    t_index *ordering_now = common::orderings_fast[s];
    point_fast d1 = pf1.degree();
    point_fast d2 = pf2.degree();

    for (unsigned int i = 0; i != common::dimension; ++i) {
        int pr = 0;
        for (unsigned int j = 0; j != common::dimension; ++j) {
            if (ordering_now[i * common::dimension + j] == 1) pr += (d1.buf[j] - d2.buf[j]);
        }
        if (pr < 0) return true;
        if (pr > 0) return false;
    }
    cout << "impossible point compare" << endl;
    for (unsigned int i = 0; i != common::dimension; ++i) cout << (int) pf1.buf[i] << ";";
    cout << endl;
    for (unsigned int i = 0; i != common::dimension; ++i) cout << (int) pf2.buf[i] << ";";
    cout << endl;
    cout << "impossible point compare" << endl;
    throw 1;// this should not happen
}


// point reference version without std
point point_reference_fast(const point_fast &v) {
    SECTOR ssector = v.sector_fast();
    unsigned short sn = common::sector_numbers_fast[ssector];
    if (sn == 0) {
        return point();
    }
    if (sn == 1) {
        return point(v, ssector);
    }

    if (common::symmetries.size() > 1) {  //there are symmetries
        vector<vector<vector<t_index> > > &sym = common::symmetries;
        point_fast best = v;
        SECTOR best_sector = ssector;
        unsigned short best_sn = sn;
        for (const auto &values : sym) {
            const vector<t_index> &permutation = values[0];
            point_fast p_new;

            for (unsigned int i = 0; i != common::dimension; ++i) { p_new.buf[i] = v.buf[permutation[i] - 1]; }

            // we only use the first part of symmetries, but I do not even know whether the other parts used to work properly
            // part 3 can be added only at parser time and means something related to part 2 = conditional symmetries
            // part 1 i odd symmetries, but they did not work properly even in earlier versions

            // now we need to compare the points and choose the lowest
            // best_sector is either common::virtual_sector or some good sector;

            SECTOR new_sector = p_new.sector_fast();
            unsigned short new_sn = common::sector_numbers_fast[new_sector];
            if (new_sn == common::virtual_sector) {
                continue; // it is a higher virtual point
            }

            if (best_sn == common::virtual_sector) { // the old one was virtual, but now a real point comes
                best_sn = new_sn;
                best_sector = new_sector;
                best = p_new;
                continue;
            }

            // here we are left with the case when both new and old are virtual; this means they are in the same sector
            if (point_fast_smaller_in_sector(p_new, best, best_sector)) {
                best = p_new;
            }
        }
        return point(best, best_sector);
    } else {
        return point(v, ssector);
    }
}


// get the right symmetry point by the vector of coordinates
point point_reference(const vector<t_index> &v) {
    SECTOR ssector = sector_fast(v);
    unsigned short sn = common::sector_numbers_fast[ssector];
    if (sn == 0) {
        return point();
    }
    if (sn == 1) {
        return point(v, 0, ssector);
    }

    if (common::symmetries.size() > 1) {  //there are symmetries
        vector<vector<vector<t_index> > > &sym = common::symmetries;
        vector<vector<t_index> > orbit;
        Orbit(v, orbit, sym);
        vector<t_index> *lowest = &(*orbit.begin());
        auto itr = orbit.begin();
        itr++;
        while (itr != orbit.end()) {
            if (is_lower_in_orbit(*itr, *lowest)) {
                lowest = &(*itr);
            }
            itr++;
        }
        return point(*lowest);
    } else {
        return point(v, 0, ssector);
    }
}

/* time to normalize an equation - to use the GCD everywhere and throw away zero members
* calls to fermat come from here
*/

#ifndef PRIME

void normalize(vector<pair<point, COEFF> > &terms, unsigned short thread_number) {
    vector<pair<point, COEFF> > mon;
    mon.reserve(terms.size());

    if (common::send_to_parent) {
        timeval start_time{}, stop_time{};
        gettimeofday(&start_time, nullptr);

        char lbuf[10];
        sprintf(lbuf, "%d\n", (int) terms.size());
        fputs(lbuf, common::child_stream_from_child);
        fflush(common::child_stream_from_child);
        char buf[sizeof(point) * 2 + 1];
        buf[sizeof(point) * 2] = '\0';
        int submitted = 0;


        auto itr = terms.begin();
        while (itr != terms.end()) {
            // we put the point
            itr->first.safe_string(buf);
            fputs(buf, common::child_stream_from_child);
            // we put the coefficient
            fputs(itr->second.s.c_str(), common::child_stream_from_child);
            fputs("\n", common::child_stream_from_child);
            fflush(common::child_stream_from_child);
            submitted++;
            ++itr;
        }

        while (submitted != 0) {
            int buf_size = 3;
            char *bbuf = (char *) malloc(static_cast<size_t>(buf_size));
            if (!bbuf) {
                cout<<"Cannot malloc in normalize"<<endl;
                throw 1;
            }

            read_from_stream(&bbuf, &buf_size, common::child_stream_to_child);

            bbuf[strlen(bbuf) - 1] = '\0';
            string ss = (bbuf + sizeof(point) * 2);
            if (ss != "0") {
                COEFF c;
                c.s = ss;
                mon.emplace_back(point(bbuf), c);
            }
            submitted--;
            free(bbuf);
        }
        gettimeofday(&stop_time, nullptr);
        __sync_fetch_and_add_8(&common::c_time, 1000000 * (stop_time.tv_sec - start_time.tv_sec) +
                                                (stop_time.tv_usec - start_time.tv_usec));

    } else {
        list<pair<int, pair<point, string> > > to_submit;
        int submitted = 0;
        auto itr = terms.begin();
        while (itr != terms.end()) {
            to_submit.emplace_back(thread_number, pair<point, string>(itr->first, itr->second.s));
            ++itr;
            submitted++;
        }

        pthread_mutex_lock(equation::f_submit_mutex + (thread_number % common::f_queues));
        for (auto &f_job : to_submit) {
            equation::f_jobs[(thread_number % common::f_queues)].push_back(f_job);
        }
        pthread_mutex_unlock(equation::f_submit_mutex + (thread_number % common::f_queues));

        for (int i = 0; i != submitted; ++i){
            sem_post(equation::f_submit_sem[(thread_number % common::f_queues)]);
        }
        to_submit.clear();

        while (submitted != 0) {
            timeval start_timeA{}, stop_timeA{};
            gettimeofday(&start_timeA, nullptr);
            sem_wait(equation::f_receive_sem[thread_number]);
            pthread_mutex_lock(equation::f_receive_mutex + thread_number);
            pair<point, string> res = *(equation::f_result[thread_number].begin());
            equation::f_result[thread_number].pop_front();
            pthread_mutex_unlock(equation::f_receive_mutex + thread_number);
            gettimeofday(&stop_timeA, nullptr);
            __sync_fetch_and_add_8(&common::thread_time, -1000000 * (stop_timeA.tv_sec - start_timeA.tv_sec) -
                                                         (stop_timeA.tv_usec - start_timeA.tv_usec));

            if (res.second != "0") {
                COEFF c;
                c.s = res.second;
                mon.emplace_back(res.first, c);
            }
            submitted--;
        }
    }
    sort(mon.begin(), mon.end(), pair_point_COEFF_smaller);
    terms = mon;
}


void normalize_eq(equation *eq, unsigned short thread_number) {
    if (common::send_to_parent) {
        timeval start_time{}, stop_time{};
        gettimeofday(&start_time, nullptr);
        char lbuf[10];
        sprintf(lbuf, "%d\n", (int) eq->length);
        fputs(lbuf, common::child_stream_from_child);
        fflush(common::child_stream_from_child);
        char buf[sizeof(point) * 2 + 1];
        buf[sizeof(point) * 2] = '\0';
        int submitted = 0;

        MONOM **itr = eq->terms;
        while (itr != eq->terms + eq->length) {
            // we put the point
            (*itr)->p.safe_string(buf);
            fputs(buf, common::child_stream_from_child);
            // we put the coefficient
            fputs((*itr)->c.s.c_str(), common::child_stream_from_child);
            fputs("\n", common::child_stream_from_child);
            fflush(common::child_stream_from_child);
            submitted++;
            ++itr;
        }

        int new_length = 0;
        while (submitted != 0) {
            int buf_size = 3;
            char *bbuf = (char *) malloc(static_cast<size_t>(buf_size));
            if (!bbuf) {
                cout<<"Cannot malloc in normalize_eq"<<endl;
                throw 1;
            }

            read_from_stream(&bbuf, &buf_size, common::child_stream_to_child);
            bbuf[strlen(bbuf) - 1] = '\0';
            string ss = (bbuf + sizeof(point) * 2);
            if (ss != "0") {
                eq->terms[new_length][0].p = point(bbuf);
                eq->terms[new_length][0].c.s = ss;
                ++new_length;
            }
            submitted--;
            free(bbuf);
        }
        eq->length = new_length;
        gettimeofday(&stop_time, nullptr);
        __sync_fetch_and_add_8(&common::c_time, 1000000 * (stop_time.tv_sec - start_time.tv_sec) +
                                                (stop_time.tv_usec - start_time.tv_usec));

    } else {
        list<pair<int, pair<point, string> > > to_submit;
        int submitted = 0;
        MONOM **itr = eq->terms;
        while (itr != eq->terms + eq->length) {
            to_submit.emplace_back(thread_number, pair<point, string>((*itr)->p, (*itr)->c.s));
            ++itr;
            submitted++;
        }

        pthread_mutex_lock(equation::f_submit_mutex + (thread_number % common::f_queues));
        for (const auto &f_job : to_submit) {
            equation::f_jobs[(thread_number % common::f_queues)].push_back(f_job);
        }
        pthread_mutex_unlock(equation::f_submit_mutex + (thread_number % common::f_queues));

        for (int i = 0; i != submitted; ++i) {
            sem_post(equation::f_submit_sem[(thread_number % common::f_queues)]);
        }
        to_submit.clear();

        int new_length = 0;
        while (submitted != 0) {
            timeval start_timeA{}, stop_timeA{};
            gettimeofday(&start_timeA, nullptr);
            sem_wait(equation::f_receive_sem[thread_number]);
            pthread_mutex_lock(equation::f_receive_mutex + thread_number);
            pair<point, string> res = *(equation::f_result[thread_number].begin());
            equation::f_result[thread_number].pop_front();
            pthread_mutex_unlock(equation::f_receive_mutex + thread_number);
            gettimeofday(&stop_timeA, nullptr);
            __sync_fetch_and_add_8(&common::thread_time, -1000000 * (stop_timeA.tv_sec - start_timeA.tv_sec) -
                                                         (stop_timeA.tv_usec - start_timeA.tv_usec));

            if (res.second != "0") {
                eq->terms[new_length][0].p = res.first;
                eq->terms[new_length][0].c.s = res.second;
                ++new_length;
            }
            submitted--;
        }
        eq->length = new_length;
    }
}
#endif


// submit to fermat evaluation queue and wait for the result
void calc_wrapper(string &s, unsigned short thread_number) {
    if (common::send_to_parent) {
        fputs("1\n", common::child_stream_from_child);
        fflush(common::child_stream_from_child);
        char buf[sizeof(point) * 2 + 1];
        buf[sizeof(point) * 2] = '\0';
        point p;
        p.safe_string(buf);

        fputs(buf, common::child_stream_from_child);
        fputs(s.c_str(), common::child_stream_from_child);
        fputs("\n", common::child_stream_from_child);
        fflush(common::child_stream_from_child);

        int buf_size = 3;
        char *bbuf = (char *) malloc(static_cast<size_t>(buf_size));
        if (!bbuf) {
            cout<<"Cannot malloc in calc_wrapper"<<endl;
            throw 1;
        }

        read_from_stream(&bbuf, &buf_size, common::child_stream_to_child);
        bbuf[strlen(bbuf) - 1] = '\0';
        s = (bbuf + sizeof(point) * 2);
        free(bbuf);
    } else {
        pthread_mutex_lock(equation::f_submit_mutex + (thread_number % common::f_queues));
        equation::f_jobs[(thread_number % common::f_queues)].emplace_back(thread_number, pair<point, string>(point(), s));
        pthread_mutex_unlock(equation::f_submit_mutex + (thread_number % common::f_queues));

        sem_post(equation::f_submit_sem[(thread_number % common::f_queues)]);
        sem_wait(equation::f_receive_sem[thread_number]);

        pthread_mutex_lock(equation::f_receive_mutex + thread_number);
        pair<point, string> res = *(equation::f_result[thread_number].begin());
        equation::f_result[thread_number].pop_front();
        pthread_mutex_unlock(equation::f_receive_mutex + thread_number);
        s = res.second;
    }
}


void *f_worker(void *par) {
    unsigned short fnum = static_cast<fermat_worker_params *>(par)->fermat_number;
    unsigned short qnum = static_cast<fermat_worker_params *>(par)->queue_number;

    while (true) {
        sem_wait(equation::f_submit_sem[qnum]);
        if (equation::f_stop) {
            break;
        }

        pthread_mutex_lock(equation::f_submit_mutex + qnum);
        int t_number = equation::f_jobs[qnum].begin()->first;
        pair<point, string> f_submit = equation::f_jobs[qnum].begin()->second;
        equation::f_jobs[qnum].pop_front();
        pthread_mutex_unlock(equation::f_submit_mutex + qnum);

        timeval start_time{}, stop_time{};
        gettimeofday(&start_time, nullptr);
        f_submit.second.erase(std::remove(f_submit.second.begin(), f_submit.second.end(), ' '), f_submit.second.end());
        f_submit.second = calc(f_submit.second.c_str(), fnum);

        if ((f_submit.second.c_str()[0] == '0') && (f_submit.second.c_str()[1] == '/')) {
            f_submit.second = "0";
        }

        gettimeofday(&stop_time, nullptr);
        __sync_fetch_and_add_8(&common::c_time, 1000000 * (stop_time.tv_sec - start_time.tv_sec) +
                                                (stop_time.tv_usec - start_time.tv_usec));

        pthread_mutex_lock(equation::f_receive_mutex + t_number);
        equation::f_result[t_number].push_back(f_submit);
        pthread_mutex_unlock(equation::f_receive_mutex + t_number);

        sem_post(equation::f_receive_sem[t_number]);
    }
    pthread_exit(nullptr);
}
